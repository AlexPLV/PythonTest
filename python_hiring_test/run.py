"""Main script for generating output.csv."""
def main():
    
    #Reading in File and Converting it into a Data Frame

    import pandas as pd

    pitch_data = pd.read_csv("./data/raw/pitchdata.csv")
    pitch_data = pd.DataFrame(pitch_data)

    #Slicing the Data Frame to filter for Pithcer Handedness

    pitch_data_Hitter_vsLHP = pitch_data.loc[pitch_data["PitcherSide"] == "L"]
    pitch_data_Hitter_vsRHP = pitch_data.loc[pitch_data["PitcherSide"] == "R"]

    #Slicing the Data Frame to filter for Hitter Handedness

    pitch_data_Pitcher_vsRHH = pitch_data.loc[pitch_data["HitterSide"] == "R"]
    pitch_data_Pitcher_vsLHH = pitch_data.loc[pitch_data["HitterSide"] == "L"]

    #Defining Fucntions to Perform Aggregations of Each Split (eg. HitterId vs RHP, PitcherTeamId vs LHH)

    def calculate_OBP(df):
        """Function used to Accept DataFrame of Hitting Stats and Return OBP as a Series"""
        num = df["H"] + df["BB"] + df["HBP"]
        den = df["AB"] + df["SF"] + df["BB"] + df["HBP"]
        return num.divide(den).round(3)
        
    def calculate_AVG(df):
        """Function that accepts DataFrame of Hitting Stats and Returns AVG as a Series"""
        AVG = pd.Series(df["H"]/df["AB"])
        return AVG.round(3)

    def calculate_SLG(df):
        """Function that acccepts DataFrame of Hittings Stats and Returns SLG as a Series"""
        SLG = pd.Series(df["TB"]/df["AB"])
        return SLG.round(3)

    def calculate_OPS(df):
        """Function that accepts DataFrame of Hittings Stats and Returns OPS as a Series"""
        num = pd.Series(df["H"] + df["BB"] + df["HBP"])
        den = pd.Series(df["AB"] + df["SF"] + df["BB"] + df["HBP"])
        OBP = num.divide(den)
        SLG = pd.Series(df["TB"]/df["AB"])
        return OBP.add(SLG).round(3)
    
    #Converting the Id of Interest into "SubjectId" and Estbalshing each Dataframe
    #H = Hitter, P = Pitcher, T = Team, IE = Inside Edge

    HvsLHP = pitch_data_Hitter_vsLHP.rename(columns = {"HitterId" : "SubjectId"})
    HvsRHP = pitch_data_Hitter_vsRHP.rename(columns = {"HitterId" : "SubjectId"})
    HTvsLHP = pitch_data_Hitter_vsLHP.rename(columns = {"HitterTeamId" : "SubjectId"})
    HTvsRHP = pitch_data_Hitter_vsRHP.rename(columns = {"HitterTeamId" : "SubjectId"})
    PvsRHH = pitch_data_Pitcher_vsRHH.rename(columns = {"PitcherId" : "SubjectId"})
    PvsLHH = pitch_data_Pitcher_vsLHH.rename(columns = {"PitcherId" : "SubjectId"})
    PTvsRHH = pitch_data_Pitcher_vsRHH.rename(columns = {"PitcherTeamId" : "SubjectId"})
    PTvsLHH = pitch_data_Pitcher_vsLHH.rename(columns = {"PitcherTeamId" : "SubjectId"})

    #Aggregating Each DataFrame and Combinations of Split and Stat

    Data_Frames = [HvsLHP, HvsRHP, HTvsLHP, HTvsRHP, PvsLHH, PvsRHH, PTvsLHH, PTvsRHH]
    Data_Frames_Edited = []
    for DF in Data_Frames:
        DF = DF.groupby("SubjectId").sum()
        DF["AVG"] = calculate_AVG(DF)
        DF["OBP"] = calculate_OBP(DF)
        DF["SLG"] = calculate_SLG(DF)
        DF["OPS"] = calculate_OPS(DF)
        DF = DF[["AVG", "OBP", "SLG", "OPS", "PA"]]
        DF = DF.loc[DF["PA"] >= 25]
        Data_Frames_Edited.append(DF)
    HvsLHP, HvsRHP, HTvsLHP, HTvsRHP, PvsLHH, PvsRHH, PTvsLHH, PTvsRHH = Data_Frames_Edited

    #Adding the Corresponding Split and Subject to Each Value
    def Split_Subject(df, Split, Subject):
        """Takes a Data Frame with the Desired Split and Subject Values and Returns these Values in New Columns"""
        df.loc[:, "Split"] = Split
        df.loc[:, "Subject"] = Subject
        return df

    Split_Subject(HvsLHP, "LHP", "HitterId")
    Split_Subject(HvsRHP, "RHP", "HitterId")
    Split_Subject(HTvsLHP, "LHP", "HitterTeamId")
    Split_Subject(HTvsRHP, "RHP", "HitterTeamId")
    Split_Subject(PvsLHH, "LHH", "PitcherId")
    Split_Subject(PvsRHH, "RHH", "PitcherId")
    Split_Subject(PTvsLHH, "LHH", "PitcherTeamId")
    Split_Subject(PTvsRHH, "RHH", "PitcherTeamId")

    #Merging Data Frames together 
    tables = [HvsLHP, HTvsLHP, HTvsRHP, PvsLHH, PvsRHH, PTvsLHH, PTvsRHH]
    F_table = HvsRHP.append(tables)

    #Transforming Final Table by Melting 

    F_table["SubjectId"] = F_table.index
    F_table_melt = pd.melt(F_table, id_vars=["SubjectId", "Split", "Subject","PA"])

    #Rearranging Column Order
    F_table_IE = F_table_melt[["SubjectId", "variable", "Split", "Subject", "value"]]

    #Renaming Columns
    column_names = ["SubjectId", "Stat", "Split", "Subject", "Value"]
    F_table_IE.columns = column_names

    #Sort DataFrame by SubjectId, Stat, Split, and Subject

    F_table_IE = F_table_IE.sort_values(["SubjectId", "Stat", "Split", "Subject"])

    #Save to CSV

    F_table_IE.to_csv("./data/processed/output.csv", index=False)
    pass

if __name__ == '__main__':
    main()



   

